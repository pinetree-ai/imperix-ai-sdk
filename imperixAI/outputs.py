# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

from typing import List, Union, Tuple, Dict, Any
from dataclasses import dataclass

# dst should match the label of one of the outputs in module.cfg
@dataclass
class ModuleOutput:
    dst: str
    timestamp: str


@dataclass
class VideoFrame(ModuleOutput):
    # TODO: replace with more specific typing for numpy.array like
    frame: Any 
    feedName: str
    frameRate: int
    pix_fmt: str


@dataclass
class Alert(ModuleOutput): 
    who: str
    priority: int
    message: str
    

@dataclass
class Geolocation(ModuleOutput): 
    entityId: str
    entityType: str
    comment: str
    latitude: Union[int,float]
    longitude: Union[int,float]
    latLngCov: Union[int,float]
    altitude: Union[int,float]
    altitudeCov: Union[int,float]
    roll: Union[int,float]
    pitch: Union[int,float]
    heading: Union[int,float]


@dataclass
class ImageOutput(ModuleOutput):
    # TODO: replace with more specific typing for numpy.array like
    srcFeed: str
    dstFeed: str
    data: Any
   

@dataclass
class Overlay: # dict for each overlay to provide flexibility to devs
    data: Dict 


@dataclass
class ImageOverlays(ModuleOutput):
    feedName: str
    overlayType: str
    overlays: List[Overlay]

    


