# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import os
import json
import socket
import asyncio
import traceback
import websockets
import subprocess
import numpy as np
from PIL import Image
from datetime import datetime
from imperixAI import protocol

LAST_TIMESTAMP = datetime.utcnow()

def create_pipe():

    return subprocess.Popen([
        
            'ffmpeg',    

            # Input
            '-loglevel', 'panic',
            "-i", '-',

            # Output 
            "tmp/video/%d.png"

        ], stdin=subprocess.PIPE)
        
in_pipe = create_pipe()

async def timestampManager():
    """
    Coroutine to keep video frames updated with timestamps
    """
    global LAST_TIMESTAMP

    while True:

        
        # Find newest frame
        frames = os.listdir('./tmp/video')
        cTimes = [ os.path.getctime(f'./tmp/video/{f}') for f in frames ]

        if len(cTimes) > 0:
            latest = frames[np.argmax(cTimes)]

            # Rename file with timestamp, ignore files that already have timestamp
            if '-' not in latest:
                f = f'./tmp/video/{latest}'
                os.rename(f, f'./tmp/video/{LAST_TIMESTAMP}.png')

        await asyncio.sleep(1 / 60)

# Packet handler
async def handlePacket(socket):
    '''
    Receive, parse, and handle packet appropriately

    Parameters
    ----------
    @param socket - connected socket
    '''

    global LAST_TIMESTAMP

    try:

        utc = datetime.utcnow()

        # Receive packet and parse
        p = await protocol.Packet.receive(socket)

        if p.pType == protocol.PacketType.TELEMETRY:

            p.__class__ = protocol.TelemetryPacket

            with open(f'tmp/telemetry/{utc}.txt', 'wb') as f:
                f.write(p.pData)


        elif p.pType == protocol.PacketType.VIDEO_CHUNK:
        
            p.__class__ = protocol.VideoPacket

            # Get video meta data
            LAST_TIMESTAMP = p.getTimestamp()

            # Pipe video stream to ffmpeg subprocess
            in_pipe.stdin.write(p.getChunk())

            
        elif p.pType == protocol.PacketType.IMAGE_START:

                p.__class__ = protocol.ImageStartPacket

                # Get complete image
                imageData = await protocol.ImageData.parse(p, socket)

                # Get image and save 
                imageTimestamp = imageData.chunks[0].getTimestamp()
                imageFeedName = imageData.chunks[0].getFeedName()
                image = Image.fromarray(imageData.getImage())

                image.save(f'./tmp/image/{imageTimestamp}.png')

    except Exception as e:

        if not hasattr(e, 'code'):
            print(e)
            print(traceback.format_exc())

            return True

        # Check for socket errors
        # 1000 - normal close
        # 1001 - going away
        # 1005 - no status code
        # 1006 - abnormal close
        if e.code in [1000, 1001, 1005, 1006]:
            print(e)
            return True

        # Capture error, and retry
        print("Packet handling failed:", socket.remote_address, " Retrying...")