# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

DATA_BUFFER_SIZE = 65536 #64kB

import os
import cv2
import ast
import time
import json
import errno
import ffmpeg
import socket
import asyncio
import subprocess
import websockets
import numpy as np
from datetime import datetime
from imperixAI import protocol
from imperixAI.outputs import ModuleOutput, ImageOutput, VideoFrame

UPDATE_FREQUENCY = 30 # Frequency for checking updates when not processing data
CONNECTION_RETRIES = 0 # Number of connection retries per attempt
CONNECTION_FAILURES = 0 # Number of connection attempts before quitting
NODE_TIMEOUT = 60 # seconds without new data before quitting

class BadOutputDestination(Exception):
    pass    

class TimedoutException(Exception):
    pass

class ImperixModule():
    """
    Read data from Imperix streamer, make predictions and return output to Imperix streamer
    """

    def __init__(self, callback, config='module.cfg'):
        """
        Load AI processing function and configure new Imperix Module

        Params
        ------
        @param callback [function or callable] - AI processing; must return a valid output
        @param config [string] - location of module.cfg file
        """

        # State management
        self.running = False

        # Check for needed tmp storage
        self.ensureTmp()

        # Save callback
        self.callback = callback
 
        # Load constants
        self.connectionRetries = CONNECTION_RETRIES
        self.connectionFailures = CONNECTION_FAILURES
        self.timeout = NODE_TIMEOUT

        # Time management
        self.lastUpdate = time.time()
        self.lastTimestamp = str(datetime.utcnow())
        
        # Video stream storage
        self.videoStreams = {}

        # Create dictionary to hold data
        self.data = {
            'Video': {'data': None, 'predicted': True},
            'Image': {'data': None, 'predicted': True},
            'Telemetry': {'data': None, 'predicted': True},
        }

        # Load config
        with open(config, 'r') as cfgFile:
            self.cfg = json.load(cfgFile)

        # Convert string to list of datatypes needed by module
        self.dataTypes = ast.literal_eval(self.cfg['DATA'])

        # Connection storage
        self.connections = {}

        # Read desired outputs from config
        for output in self.cfg['OUTPUTS']:            
            self.connections[output['label']] = {
                'socket': None,
                'uri': output['uri']
            }      

    def ensureTmp(self):
        """
        Check if the needed filesystem is available for tmp storage
        """

        for _dir in ['tmp', 'video', 'telemetry', 'image']:

            if _dir != 'tmp':
                _dir = f'tmp/{_dir}'

            if os.path.isdir(_dir):
                pass
            else:
                os.mkdir(_dir)

    async def start(self):
        """
        Main call to start Imperix Module
        """

        self.running = True

        while self.running:
            
            # Try to connect if any of the desired outputs are none
            notConnected = any(connection['socket'] is None for connection in self.connections.values())

            if notConnected: # Ensure we stay connected
                
                print("Attempting to connect to socket")
                
                if not await self.connect():                                        
                    self.connectionRetries += 1
                    await asyncio.sleep(1) # Wait and try again

                    # If we exceed 3 retries, re-connect
                    if self.connectionRetries >= 3:
                        print("\nTrouble connecting to Imperix Cloud. Sleeping for 15 seconds before retrying...\n")
                        self.connectionRetries = 0
                        self.connectionFailures += 1

                        # If we fail to establish a connection end program
                        if self.connectionFailures >= 3:
                            raise TimedoutException('Failed to establish a connection. Ending Imperix Module.')

                        await asyncio.sleep(15)            

            # Once connected, start processing data
            else:

                start = time.time()

                self.connectionFailures = 0
                self.connectionRetries = 0
                await self.processData()

                # Check for timeout
                now = time.time()
                if (now - self.lastUpdate) > self.timeout:
                    raise TimedoutException('Too long from last update. Shutting Down.')

                await asyncio.sleep(1 / UPDATE_FREQUENCY)

                end = time.time()
                freq = 1 / (end - start)

                print(f'Processing at {freq:.2f}hz      ', end='\r', flush=True)

    async def stop(self):

        self.running = False
        self.videoStreams = {}
        self.connections = {}

    async def connect(self):
        """
        Imperix Module connection handler
        ---------------------------------

        1. Only connect to streamer if connection doesn't exist
        2. Send an auth packet on succesful connection
        """

        success = True

        p = protocol.AuthPacket.constructFromAuth(
                self.cfg['NODE_UUID'],
                self.cfg['ACCESS_KEY'],
                isNode=False,
                moduleId=self.cfg["MODULE_ID"],
                isModule=True
            )

        # Iterate through all desired connections
        for output in self.connections.keys():
            try:
                connection = self.connections[output]
                
                # Only attempt to connect to those that are not connected
                if connection['socket'] is None:
                    socket = await websockets.connect(connection['uri'])

                    #transmit auth packet
                    await p.transmit(socket)

                    # On successful auth; save socket
                    self.connections[output]['socket'] = socket

                    print(f'Connected to {output}')

            except Exception as e:
                print(f"Failed to connect to {output}")
                print(e)
                success = False

        return success    

    async def processData(self):
        """
        Data processing pipeline
        ------------------------
        1. Check tmp storage for new data
        2. Add new data to self.data
        3. Run callback function when requested data is available
        4. If the callback returns outputs, call self.streamOutputs
        """

        # Check for new video frames              
        if len(os.listdir('./tmp/video')) > 0:     

            # Update video data
            self._handleNewVideoData()

            # Flush video frames
            os.system('rm ./tmp/video/*') 

        # Check for new images
        if len(os.listdir('./tmp/image')):          

            # Update image data
            self._handleNewImageData()
            
            # Flush images
            os.system('rm ./tmp/image/*')

        # Check for new telemetry
        if len(os.listdir('./tmp/telemetry')) > 0:

            # Update telemetry data
            self._handleNewTelemetryData()   

            # Flush images
            os.system('rm ./tmp/telemetry/*')  

        # After data has been updated pass to callback function
        await self.callbackHandler()  

    async def callbackHandler(self):
        """
        Pass only the requested data onto the module callback function
        """
        # Run callback if video is only dataType
        if len(self.dataTypes) == 1 and 'Video' in self.dataTypes:

            output = self.callback(self.data['Video'])

            if output is not None and len(output) > 0:
                await self.streamOutput(output)   

        # Run callback if image is only dataType
        if len(self.dataTypes) == 1 and 'Image' in self.dataTypes:
            output = self.callback(self.data['Image'])

            if output is not None and len(output) > 0:
                self.streamOutput(output)

        # Run callback if telemetry is only dataType
        if len(self.dataTypes) == 1 and 'Telemetry' in self.dataTypes:

            output = self.callback(self.data['Telemetry'])

            if output is not None and len(output) > 0:
                await self.streamOutput(output)   

        # For cases with more than one datatype
        if len(self.dataTypes) > 1:
            if any(not self.data[key]['predicted'] for key in self.data.keys()): # only update if new data comes in

                output = self.callback(self.data)

                if output is not None and len(output) > 0:
                    await self.streamOutput(output)  

                # Update data dict to reflect that predictions have been made
                for key in self.data.keys():
                    self.data[key]['predicted'] = True      

    async def streamOutput(self, output):
        """
        Send outputs back to Imperix Streamer
        -------------------------------------

        1. Iterate through all outputs returned by callback 
        2. Outputs are sent to the specific destination (dst)
        """

        # Stream all outputs
        for out in output:

            if type(out) == ImageOutput:
                await self.transmitImage(out)

            elif type(out) == VideoFrame:
                await self.transmitVideoFrame(out)

            # Any remaining outputs will be sent as json
            elif isinstance(out, ModuleOutput):
                await self.transmitData(out)

    def _handleNewVideoData(self):
        # Find newest frame
        frames = os.listdir('./tmp/video')
        cTimes = [ os.path.getctime(f'./tmp/video/{f}') for f in frames ]
        latest = frames[np.argmax(cTimes)]

        try:
            frame = cv2.imread(f'./tmp/video/{latest}')
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

            # Get timestamp from filename
            timestamp = latest[:-4]

            self.data['Video'] = {
                'data': frame, 
                'timestamp': timestamp,
                'predicted': False
                }

        except cv2.error:
            # Error can occur when module tries to load a file before it is renamed
            pass 

    def _handleNewTelemetryData(self):
        # Find latest telemetry
        telemetry = os.listdir('./tmp/telemetry')
        cTimes = [ os.path.getctime(f'./tmp/telemetry/{f}') for f in telemetry ]
        latest = telemetry[np.argmax(cTimes)]
        
        with open(f'./tmp/telemetry/{latest}', 'r') as f:
            data = json.loads(f.read())

        self.data['Telemetry'] = {'data': data, 'predicted': False}

    def _handleNewImageData(self):
        # Find newest frame
        images = os.listdir('./tmp/image')
        cTimes = [ os.path.getctime(f'./tmp/image/{f}') for f in images ]
        latest = images[np.argmax(cTimes)]

        image = cv2.imread(f'./tmp/image/{latest}.png')
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        self.data['Image'] = {'data': image, 'predicted': False}    

    async def transmitImage(self, imageOutput):
        # Ensure we send a valid timestamp
        if '-' not in imageOutput.timestamp:
            imageOutput.timestamp = self.lastTimestamp
        else:
            self.lastTimestamp = imageOutput.timestamp

        p = protocol.ImageData.constructFromImage(
            imageOutput.data, 
            imageOutput.timestamp,
            imageOutput.dstFeed
        )

        # Determine which socket to transmit over
        try:
            socket = self.connections[imageOutput.dst]['socket']
        except KeyError:
            raise BadOutputDestination('Invalid output. Destination must be defined in module.cfg')

        # Send packet
        try:
            await p.transmit(socket)
            self.lastUpdate = time.time()

        except Exception as e:

            # If there is a socket error; reset the socket connection
            if hasattr(e, 'code'):
                print("Packet handling failed:", socket.remote_address, " Retrying...")

                # Remove socket so we reconnect on next loop
                self.connections[imageOutput.dst]['socket'] = None

    async def transmitData(self, dataOutput):
        # Ensure we send a valid timestamp
        if '-' not in dataOutput.timestamp:
            dataOutput.timestamp = self.lastTimestamp
        else:
            self.lastTimestamp = dataOutput.timestamp

        # Create packet
        p = protocol.DataPacket()
        p.setData(dataOutput.__dict__) 

        # Determine which socket to transmit over
        try:
            socket = self.connections[dataOutput.dst]['socket']
        except KeyError:
            raise BadOutputDestination('Invalid output. Destination must be defined in module.cfg')

        # Send packet
        try:
            await p.transmit(socket, catch=True)
            self.lastUpdate = time.time()

        # If there is a socket error; reset the socket connection
        except Exception as e:
            if hasattr(e, 'code'):
                print("Packet handling failed:", socket.remote_address, " Retrying...")
                
                # Remove socket so we reconnect on next loop
                self.connections[dataOutput.dst]['socket'] = None

    async def transmitVideoFrame(self, videoFrame):
        '''
        Stream live video from source - file or camera.
        Stream ends (and encoding subprocess is killed) at end of file, or if camera stream is empty.
        '''

        # Get data from VideoFrame object
        frame = videoFrame.frame
        pix_fmt = videoFrame.pix_fmt
        frameRate = videoFrame.frameRate
        feedName = videoFrame.feedName
        output = videoFrame.dst

        # If already running; just update frame
        if feedName in self.videoStreams and self.videoStreams[feedName]['running']:
            self.videoStreams[feedName]['frame'] = frame

        else:
            # Start encoding subprocess, add to map
            udpPort = 9000

            # Start UDP socket
            self.videoStreams[feedName] = {}
            self.videoStreams[feedName]["output"] = output
            self.videoStreams[feedName]["UDP"] = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

            # Try to bind on port until success
            for _ in range(10): # Max 10 attempts/streams
                try:
                    self.videoStreams[feedName]["UDP"].bind(("0.0.0.0", udpPort))
                    self.videoStreams[feedName]['udpPort'] = udpPort
                    break # Success
                    
                except:
                    udpPort += 1 # Try next port
            
            # Start UDP server to handle FFMPEG stream
            self.videoStreams[feedName]["KILL"] = False

            # Asyncio coroutine
            asyncio.ensure_future(self.udpServer(feedName))

            # Start stream
            self.videoStreams[feedName]['frame'] = frame
            asyncio.ensure_future(self.streamHandler(feedName, pix_fmt, frameRate))

    async def streamHandler(self, feedName, pix_fmt, frameRate):
        """
        Coroutine to handle asyncronously generated frames from a module.
        Streams are received by UDP server and packaged into Imperix VIDEO_CHUNKS
        to be forwarded to the rest of Imperix
        """
        # Start stream
        self.videoStreams[feedName]['running'] = True

        # Create ffmpeg subprocess
        shape = self.videoStreams[feedName]['frame'].shape
        port = self.videoStreams[feedName]['udpPort']

        _input = ffmpeg.input("pipe:0",
                          format='rawvideo',
                          pix_fmt=pix_fmt,
                          video_size=(shape[1], shape[0]),
                          framerate=frameRate)
    
        _output = ffmpeg.output(_input,
                            f'udp://127.0.0.1:{port}',
                            format='mpegts',
                            vcodec='mpeg1video')

        self.videoStreams[feedName]['proc'] = ffmpeg.run_async(_output, 
                                                               pipe_stdin=True, 
                                                               quiet=True)

        # Main stream loop
        while self.videoStreams[feedName]['running']:

            frame = self.videoStreams[feedName]['frame'] 

            if frame is None:
                # Stop UDP server
                self.videoStreams[feedName]['KILL'] = True

                # Stop subprocess
                self.videoStreams[feedName]['proc'].terminate()

                # Stop stream loop
                self.videoStreams[feedName]['running'] = False
                break

            # Pipe new frames to ffmpeg via stdin
            self.videoStreams[feedName]['proc'].stdin.write(frame.tobytes())

            await asyncio.sleep(1 / frameRate)

    async def udpServer(self, feedName):
        """
        Starts a UDP server to receive video streams from ffmpeg subprocess.
        Stream bytes are packaged into Imperix VIDEO_CHUNKs and forwarded to the
        desired output
        """
        sock = self.videoStreams[feedName]["UDP"]
        sock.setblocking(0) # Non-blocking call

        output = self.videoStreams[feedName]['output']

        # Run until either all node threads are killed, or specific feed is killed
        while not self.videoStreams[feedName]["KILL"]:

            try:
                rx,_ = sock.recvfrom(DATA_BUFFER_SIZE)
            
            except socket.error as e:
                if e.errno != errno.EAGAIN:
                    raise e

                await asyncio.sleep(0.01)
                continue

            except Exception as e:
                print(e)
                # TODO - handle broken connection
                break
            
            p = protocol.VideoPacket.constructFromChunk(rx, feedName=feedName)
            await p.transmit(self.connections[output]['socket'])
            
            



        



        
