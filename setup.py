# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import setuptools

with open("README.md", "r") as handle:
    long_description = handle.read()

setuptools.setup(
    name = "imperix-AI",
    version = "0.2.5",
    description = "Imperix AI SDK including the Imperix Modules which provide AI processing to live robot and drone streams.",
    long_description=long_description,
    long_description_content_type = "text/markdown",
    author = "Aptus Engineering Inc.",
    author_email = "software@aptusai.com",
    url = "https://bitbucket.org/pinetree-ai/imperix-ai-sdk/",
    packages=setuptools.find_packages(),
    classifiers = [
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires = '>=3.6',
    install_requires=[
        'asyncio',
        'motor',
        'numpy',
        'opencv-python',
        'Pillow',
        'requests',
        'websockets'
    ]
)