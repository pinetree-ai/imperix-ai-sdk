# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import numpy as np

def m2geoDeg(m):
    return m / 1852 / 60

def geoDeg2m(d):
    return d * 60 * 1852

def deg2rad(d):
    return d * np.pi / 180

def rad2deg(r):
    return r * 180 / np.pi

def m2ft(m):
    return m * 3.28084

def radWrap(r, s=0):
    while r > s + np.pi:
        r -= 2 * np.pi

    while r < s - np.pi:
        r += 2 * np.pi

    return r