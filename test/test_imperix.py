# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import os
import time
import json
import random
import asyncio
import websockets
from datetime import datetime
from imperixAI import (ImperixModule, protocol, Alert, handlePacket, 
                       TimedoutException, VideoFrame)

# Global success var to simplify handling async testing
SUCCESS = False
CONNECTED = False

"""
Dummy callback for imperixAI testing
"""

def badGuyDetector(data):

    global SUCCESS
    
    # randomly determine success
    detected = random.random() > 0.5

    if detected:

        # Succesfully read data and found bad guy
        SUCCESS = True

        # Return empty list to avoid trying to stream output
        return []

"""
Create module for testing
"""
imperixModule = ImperixModule(badGuyDetector, config='test/module_test.cfg')


def test_telemetry():
    """
    Test for modules ability to read incoming telemetry data and return
    a valid output
    """

    # Update datatypes so that module only accepts telemetry data
    imperixModule.dataTypes = ['Telemetry']

    # Load data
    telemetry_file = 'test/data/2020-05-29 16:55:46.189767.txt'

    # Pseudo-stream telemetry to tmp directory
    async def streamTel():

        global SUCCESS

        i = 0

        # Copy telemetry file to tmp to simulate streamed data 
        while not SUCCESS:

            i += 1

            with open(telemetry_file, 'rb') as f:

                with open(f'tmp/telemetry/test{i}.txt', 'wb') as new_f:
                    new_f.write(f.read())

            await asyncio.sleep(1 / 30)


    # Listen for a valid response from module
    async def moduleProcess():        

        start = time.time()

        while not SUCCESS:

            if (time.time() - start) > 60: #timeout after 1 minute
                raise Exception('Failed to load telemetry data')

            # Start module check for data 
            await imperixModule.processData()

            await asyncio.sleep(1 / 30) # check at 30hz


    asyncio.get_event_loop().run_until_complete(asyncio.gather(
        streamTel(),
        moduleProcess()
    ))

    """
    Finally, reset success for further testing
    """

    global SUCCESS
    assert SUCCESS
    SUCCESS = False


def test_video():
    """
    Test for modules ability to read incoming video data and return
    a valid output
    """

    # Update datatypes so that module only accepts telemetry data
    imperixModule.dataTypes = ['Video']

    # Load data
    image_file = 'test/data/top_secret.jpeg'

    # Pseudo-stream video to tmp directory (copying an image into tmp)
    async def streamVideo():

        global SUCCESS

        i = 0

        while not SUCCESS:

            i += 1

            os.system(f'cp {image_file} tmp/video/{i}.jpeg')

            await asyncio.sleep(1 / 5) # add frames at 5hz


    # Listen for a valid response from module
    async def moduleProcess():        

        start = time.time()

        while not SUCCESS:

            if (time.time() - start) > 60: #timeout after 1 minute
                raise Exception('Failed to load video data')

            # Start module check for data 
            await imperixModule.processData()

            await asyncio.sleep(1 / 30) # check at 30hz


    asyncio.get_event_loop().run_until_complete(asyncio.gather(
        streamVideo(),
        moduleProcess()
    ))

    """
    Finally, reset success for further testing
    """

    global SUCCESS
    assert SUCCESS
    SUCCESS = False
    

def test_stream():

    def callback(data):

        alert = Alert(
            dst='STREAMER',
            timestamp='',
            who='Tux the Penguin',
            priority=1,
            message='It\'s actually GNU/Linux'
        )

        # Return list that will be sent to STREAMER
        return [alert]

    # Patch in our new callback function
    imperixModule.callback = callback 

    async def streamer(socket, path):

        global CONNECTED
        global SUCCESS

        CONNECTED = True

        while not SUCCESS:

            p = await protocol.Packet.receive(socket)

            if p.pType == protocol.PacketType.STREAM_DATA:
                SUCCESS = True
                break

    async def moduleProcess():

        global CONNECTED
        global SUCCESS

        while not CONNECTED:
            try:
                socket = await websockets.connect('ws://localhost:5003')
                imperixModule.connections['STREAMER']['socket'] = socket
            except ConnectionRefusedError:
                await asyncio.sleep(1)

        # Transmit a single occurence of output from our callback
        output = imperixModule.callback(None)
        await imperixModule.streamOutput(output)

        # Rest connections
        imperixModule.connections['STREAMER']['socket'] = None

        # Give the streamer time to receive and process the packet
        while not SUCCESS:
            await asyncio.sleep(1)
            

    asyncio.get_event_loop().run_until_complete(asyncio.gather(
        websockets.serve(streamer, 'localhost', 5003),
        moduleProcess()       
    ))

    """
    Finally, reset success for further testing
    """
    global SUCCESS
    assert SUCCESS
    SUCCESS = False

def testTimeout():
    """
    Imperix modules will timeout after not connecting to all outputs.
    """

    global SUCCESS
    
    async def main():

        global SUCCESS

        imperixModule.connectionFailures = 2

        try:
            await asyncio.wait_for(imperixModule.start(), 90)
        
        # This is an imperixAI exception, indicating a succesful timeout from the module
        except TimedoutException: 
            SUCCESS = True

    asyncio.get_event_loop().run_until_complete(
        main()
    )    

    assert SUCCESS

    """
    Finally, reset success for further testing
    """
    SUCCESS = False

    