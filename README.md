# Imperix AI SDK 

## Overview

**Imperix AI SDK** including the **Imperix Module** which provides AI processing to live robot and drone streams.

The core functionality of the **SDK** includes:

* Authentication with the **Imperix Cloud**
* A framework for building AI modules that plug directly into **Imperix**
* Processing of telemetry, video, and image data from live streams
* Allows developers to focus purely on the AI components of their modules

## Installation

All dependencies for the **Imperix Node SDK** Python implementation are provided in the *requirements.txt* file. To install the dependencies, run the following command:

```sh
pip3 install -r requirements.txt
```

### Configuration 

Setup requires a module.cfg files which specifies the following:

* Unique node identifier string
* Node authorization access key provided upon node registration
* Unique module identifier string
* Desired data types for processing [Video, Image, or Telemetry]
* URI information for the **Imperix Streamer**
* An example module.cfg file can be found in the example-module repo

### Setup 

A new **Imperix** Module can be created simply by importing **Imperix**Module and instantiation. Each module will require a callback function (or callable) to be passed that will be called whenever new data is available. The data passed to the callable is defined in the configuration file. Callables should return a list of predefined **Imperix** output dataclasses to return to the streamer or the supervisor. 

### Usage 

The **Imperix AI SDK** is intended to allow developers to easily create new Imperix Modules. Take a look at the code below to see how simple creating a new Imperix Module can be. The SDK also provides useful functions for handling data streams. As you will see below, we will use the *handlePacket* function to process the incoming data and the *timestampManager* to ensure our live video frames are saved with a proper timestamp.

```python
# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import random
import asyncio
import websockets
from datetime import datetime
from imperixAI import ImperixModule, handlePacket, timestampManager, Alert

# Define our callback function (this is where the AI happens!)
def badGuyDetector(data):

    detected = random.random() > 0.95

    if detected:

        output = []

        # Create an alert output
        alert = Alert(
            who='Bad Guy',
            priority=3,
            message='We found a bad guy',
            dst='streamer',
            timestamp=str(datetime.now())
        ) 

        output.append(alert)

        # Return list of outputs to be forwarded back to streamer
        return output
        
# Instantiate new module
module = ImperixModule(badGuyDetector, config='module.cfg')

# Listen for data incoming from streamer
async def main(socket, patch):

    print("Incomming connection from:", socket.remote_address)

    while True:     

        # Handle packet data
        if not await handlePacket(socket):
        else:
            break

    print("Client disconnected:", socket.remote_address)


host = module.cfg['MODULE_HOST']
port = int(module.cfg['MODULE_PORT'])

loop = asyncio.get_event_loop()

try:
    loop.run_until_complete(asyncio.gather(
        websockets.serve(main, host, port),
        module.start(),
        timestampManager()
    ))
    loop.run_forever()

except KeyboardInterrupt:
    pass

finally:
    loop.close()
```

### Writing your callback function

The *callback* function provided by the developer will determine the core functionality of the new **Imperix Module**. The module's job is to wait for new data to be received, pass it to the callback function, and to stream the output back to the Imperix Cloud. The data that will be passed to your callback function will be determined by the *DATA* argument in the module.cfg file. You will need to ensure that your callback function returns one of the following valid outputs:

* Alert
* Geolocation
* ImageOutput
* ImageOverlays
* Data (JSON serializable dictionary)

