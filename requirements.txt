ffmpeg==1.4
motor==2.1.0
numpy==1.18.2
opencv-python==4.2.0.34
Pillow==7.0.0
pymongo==3.10.1
pyparsing==2.4.6
python-engineio==3.11.2
six==1.14.0
setuptools==44.0.0
typing-extensions==3.7.4.1
wcwidth==0.1.8
python-dotenv==0.12.0
websockets==8.1
